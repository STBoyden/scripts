#!/bin/python

import subprocess

def main():
    cmd = "pgrep polybar"
    is_open = False
    # output = str(subprocess.check_output(cmd, shell=True))
    proc = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = proc.communicate()
    output = str(output)
    output = list(output)

    while output == ["b", "'", "'"]:
        print("Polybar open?: {0}".format(is_open))
        subprocess.Popen("/home/samuel/Scripts/pb-launch")
        exit(0)

    if output != []:
        is_open = True
    print("Polybar open?: {0}".format(is_open))

main()
